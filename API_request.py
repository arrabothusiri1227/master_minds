import requests as rq
import json 

session = rq.Session()

def get_url():
    url = "https://we6.talentsprint.com/wordle/apidocs/"
    response = session.get(url)
    return response.status_code

def url_register(name: str) -> str:
    post_url = "https://we6.talentsprint.com/wordle/game/register"
    request = {"mode": "mastermind", "name": name}
    response = session.post(post_url, json = request)
    return response.json()['id']

def url_create() -> bool:
    post_url = "https://we6.talentsprint.com/wordle/game/create"
    request = {"id": f'{url_register("vaishnavi")}', "overwrite": True}
    response = session.post(post_url, json = request)
    return (request["id"], response.json()["created"])

def play_game(guess: str) -> int:
    post_url = "https://we6.talentsprint.com/wordle/game/guess"
    request = {"guess": guess, "id": f'{url_create()[0]}'}
    if url_create():
        response = session.post(post_url, json = request)
    return response.json()["feedback"]

print(play_game("apple"))
